# helping-hands

A handy little app that does some handy stuff.

[Rust]: https://www.rust-lang.org/

## Development

### Install prerequisites

- [Rust]
- [pre-commit]

[pre-commit]: https://pre-commit.com/

### Install the pre-commit hook

```sh
pre-commit install
```

This installs a Git hook that runs a quick sanity check before every commit.

### Run the app

```sh
cargo run
```

### Run the tests

```sh
cargo test
```

### Run a smoke test

```sh
cargo test -- --ignored --nocapture <pattern>
```
