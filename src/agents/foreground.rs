use crate::{
    agents::Agent,
    broker::Broker,
    signal::Signal,
    windows::{
        foreground_window_hook::{Callback, ForegroundWindowHook},
        utils::get_exe_for_hwnd,
    },
};
use tracing::error;
use winapi::shared::windef::HWND;

pub struct ForegroundWindowAgent;

impl ForegroundWindowAgent {
    pub fn new(broker: Broker) -> Self {
        let callback = ForegroundWindowCallback::new(broker);
        ForegroundWindowHook::new(callback);
        Self
    }
}

impl Agent for ForegroundWindowAgent {}

struct ForegroundWindowCallback {
    broker: Broker,
}

impl ForegroundWindowCallback {
    fn new(broker: Broker) -> Self {
        Self { broker }
    }
}

impl Callback for ForegroundWindowCallback {
    fn foreground_window_changed(&mut self, hwnd: HWND) {
        match get_exe_for_hwnd(hwnd) {
            Ok(exe) => self.broker.enqueue(Signal::ForegroundWindowChanged { exe }),
            Err(err) => error!(error = ?err, "error getting exe for foreground window"),
        }
    }
}
