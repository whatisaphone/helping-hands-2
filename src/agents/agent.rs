use crate::signal::Signal;

pub trait Agent {
    fn handle(&mut self, _signal: &Signal) {}
}
