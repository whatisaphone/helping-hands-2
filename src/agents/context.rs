use crate::{
    agents::{Agent, Sound, Tint},
    broker::Broker,
    signal::Signal,
    windows::inputsim::{MouseButton, VirtualKey},
};
use smallvec::SmallVec;
use std::path::Path;
use winapi::um::winuser::{
    VK_CONTROL,
    VK_LEFT,
    VK_LWIN,
    VK_MENU,
    VK_NEXT,
    VK_PRIOR,
    VK_RIGHT,
    VK_SHIFT,
    VK_TAB,
};

pub struct ContextAgent {
    broker: Broker,
    context: Context,
}

impl ContextAgent {
    pub fn new(broker: Broker) -> Self {
        Self {
            broker,
            context: Context::default(),
        }
    }
}

impl Agent for ContextAgent {
    fn handle(&mut self, signal: &Signal) {
        match *signal {
            Signal::ForegroundWindowChanged { ref exe } => {
                self.context = Context::for_exe(Path::new(exe));
                self.broker.enqueue(Signal::SetTint {
                    tint: self.context.tint,
                });
            }
            Signal::MouseWheelLeft => {
                self.broker.enqueue(Signal::KeyboardChord {
                    vks: self.context.mouse_wheel_left_keys,
                });
            }
            Signal::MouseWheelRight => {
                self.broker.enqueue(Signal::KeyboardChord {
                    vks: self.context.mouse_wheel_right_keys,
                });
            }
            Signal::JoyButtonDown { button } => {
                if self.context.gamepad == GamepadMapping::Normal && button == 1 {
                    self.broker.enqueue(Signal::CycleTint);
                }
            }
            Signal::TataSound(edge, sound) => {
                if self.context.gamepad != GamepadMapping::Normal {
                    return;
                }
                match sound {
                    Sound::T => {
                        self.broker
                            .enqueue(Signal::SimulateMouseButton(MouseButton::Left, edge.into()));
                    }
                    Sound::S => {
                        self.broker
                            .enqueue(Signal::SimulateMouseButton(MouseButton::Right, edge.into()));
                    }
                    Sound::K => {
                        self.broker.enqueue(Signal::SimulateMouseButton(
                            MouseButton::Middle,
                            edge.into(),
                        ));
                    }
                }
            }
            _ => {}
        }
    }
}

struct Context {
    tint: Tint,
    mouse_wheel_left_keys: &'static [VirtualKey],
    mouse_wheel_right_keys: &'static [VirtualKey],
    gamepad: GamepadMapping,
}

#[derive(Eq, PartialEq)]
enum GamepadMapping {
    Ignore,
    Normal,
}

const CTRL_PAGEUP: &[VirtualKey] = &[VK_CONTROL, VK_PRIOR];
const CTRL_PAGEDOWN: &[VirtualKey] = &[VK_CONTROL, VK_NEXT];
const CTRL_SHIFT_TAB: &[VirtualKey] = &[VK_CONTROL, VK_SHIFT, VK_TAB];
const CTRL_TAB: &[VirtualKey] = &[VK_CONTROL, VK_TAB];
const ALT_LEFT: &[VirtualKey] = &[VK_MENU, VK_LEFT];
const ALT_RIGHT: &[VirtualKey] = &[VK_MENU, VK_RIGHT];
const WIN_Q: &[VirtualKey] = &[VK_LWIN, 'Q' as VirtualKey];
const WIN_SHIFT_Q: &[VirtualKey] = &[VK_LWIN, VK_SHIFT, 'Q' as VirtualKey];

impl Context {
    fn default() -> Self {
        Self {
            tint: Tint::Red,
            mouse_wheel_left_keys: CTRL_SHIFT_TAB,
            mouse_wheel_right_keys: CTRL_TAB,
            gamepad: GamepadMapping::Normal,
        }
    }

    fn for_exe(exe: &Path) -> Self {
        let components: SmallVec<[_; 32]> = exe
            .components()
            .map(|c| c.as_os_str().to_str().unwrap_or("<weird_filename>"))
            .collect();

        let stem = match exe.file_stem() {
            None => return Self::default(),
            Some(stem) => stem.to_str().unwrap_or("<weird_filename>"),
        };

        if regex!("(?i)^(?:clion|idea|pycharm|studio|webstorm)(?:64)?$").is_match(&stem) {
            Self {
                tint: Tint::Pink,
                mouse_wheel_left_keys: ALT_LEFT,
                mouse_wheel_right_keys: ALT_RIGHT,
                gamepad: GamepadMapping::Normal,
            }
        } else if stem.eq_ignore_ascii_case("code") {
            // Visual Studio Code
            Self {
                tint: Tint::Pink,
                mouse_wheel_left_keys: CTRL_PAGEUP,
                mouse_wheel_right_keys: CTRL_PAGEDOWN,
                gamepad: GamepadMapping::Normal,
            }
        } else if stem.eq_ignore_ascii_case("conemu64") {
            Self {
                tint: Tint::Pink,
                mouse_wheel_left_keys: WIN_Q, // ConEmu's default shortcuts are seriously wonky
                mouse_wheel_right_keys: WIN_SHIFT_Q,
                gamepad: GamepadMapping::Normal,
            }
        } else if stem.eq_ignore_ascii_case("paintdotnet")
            || stem.eq_ignore_ascii_case("vlc")
            || regex!("(?i)^gimp").is_match(stem)
        {
            Self {
                tint: Tint::Reset,
                mouse_wheel_left_keys: CTRL_SHIFT_TAB,
                mouse_wheel_right_keys: CTRL_TAB,
                gamepad: GamepadMapping::Normal,
            }
        } else if components.iter().any(|c| c.eq_ignore_ascii_case("steam")) {
            Self {
                tint: Tint::Reset,
                mouse_wheel_left_keys: CTRL_SHIFT_TAB,
                mouse_wheel_right_keys: CTRL_TAB,
                gamepad: GamepadMapping::Ignore,
            }
        } else {
            Self::default()
        }
    }
}

#[cfg(test)]
mod tests {
    use crate::{
        agents::{Agent, ContextAgent, Tint},
        broker::Broker,
        signal::Signal,
    };

    #[test]
    fn when_foreground_is_normal_app_then_should_set_tint_to_red() {
        let (tx, rx) = crossbeam_channel::unbounded();
        let mut subject = ContextAgent::new(Broker::new(tx));

        subject.handle(&Signal::ForegroundWindowChanged {
            exe: r"C:\Windows\notepad.exe".to_owned(),
        });

        match rx.try_recv() {
            Ok(Signal::SetTint { tint: Tint::Red }) => (),
            _ => panic!(),
        };
    }

    #[test]
    fn when_foreground_is_ide_then_should_set_tint_to_pink() {
        let (tx, rx) = crossbeam_channel::unbounded();
        let mut subject = ContextAgent::new(Broker::new(tx));

        subject.handle(&Signal::ForegroundWindowChanged {
            exe: r"C:\Program Files\JetBrains\PyCharm 2017.1.5\bin\pycharm.exe".to_owned(),
        });

        match rx.try_recv() {
            Ok(Signal::SetTint { tint: Tint::Pink }) => (),
            _ => panic!(),
        };
    }

    #[test]
    fn when_foreground_is_normal_app_then_gamepad_should_work() {
        let (mut subject, rx) = build_subject_with_app(r"C:\Windows\notepad.exe");

        subject.handle(&Signal::JoyButtonDown { button: 1 });

        match rx.try_recv() {
            Ok(Signal::CycleTint) => (),
            _ => panic!(),
        };
    }

    #[test]
    fn when_foreground_is_game_then_gamepad_should_be_ignored() {
        let (mut subject, rx) = build_subject_with_app(r"C:\Steam\RocketLeague.exe");

        subject.handle(&Signal::JoyButtonDown { button: 1 });

        match rx.try_recv() {
            Err(crossbeam_channel::TryRecvError::Empty) => (),
            _ => panic!(),
        };
    }

    fn build_subject_with_app(exe: &str) -> (ContextAgent, crossbeam_channel::Receiver<Signal>) {
        let (tx, rx) = crossbeam_channel::unbounded();
        let mut subject = ContextAgent::new(Broker::new(tx));

        subject.handle(&Signal::ForegroundWindowChanged {
            exe: exe.to_owned(),
        });

        // Expect the subject to set the tint according to the foreground window.
        match rx.try_recv() {
            Ok(Signal::SetTint { .. }) => (),
            _ => panic!(),
        };

        // Ensure we return an empty queue.
        match rx.try_recv() {
            Err(crossbeam_channel::TryRecvError::Empty) => (),
            _ => panic!(),
        };

        (subject, rx)
    }
}
