use crate::{
    agents::Agent,
    signal::Signal,
    windows::inputsim::{send_chord, send_mouse},
};

pub struct InputSimulateAgent;

impl InputSimulateAgent {
    pub fn new() -> Self {
        Self
    }
}

impl Agent for InputSimulateAgent {
    fn handle(&mut self, signal: &Signal) {
        match signal {
            Signal::KeyboardChord { vks } => {
                send_chord(vks);
            }
            &Signal::SimulateMouseButton(button, direction) => {
                send_mouse(button, direction);
            }
            _ => {}
        }
    }
}
