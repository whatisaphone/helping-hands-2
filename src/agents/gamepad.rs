use crate::{agents::Agent, broker::Broker, signal::Signal};
use sdl2::event::Event;
use std::{error::Error, thread};
use tracing::error;

pub struct GamepadAgent;

impl GamepadAgent {
    pub fn new(broker: Broker) -> Self {
        thread::spawn(move || {
            match gamepad_thread(&broker) {
                Ok(()) => {}
                Err(err) => error!(error = ?err, "gamepad error"),
            }
        });
        Self
    }
}

impl Agent for GamepadAgent {}

fn gamepad_thread(broker: &Broker) -> Result<(), Box<dyn Error>> {
    let sdl2 = sdl2::init()?;
    let joysticks = sdl2.joystick()?;
    let _joystick = joysticks.open(0)?;
    for event in sdl2.event_pump()?.wait_iter() {
        match event {
            Event::JoyButtonDown { button_idx, .. } => {
                broker.enqueue(Signal::JoyButtonDown { button: button_idx });
            }
            _ => (),
        }
    }
    Ok(())
}
