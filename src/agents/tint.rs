use crate::{agents::Agent, config, signal::Signal};
use std::{
    os::windows::process::CommandExt,
    process::{Command, Stdio},
};
use tracing::error;
use winapi::um::winbase::CREATE_NO_WINDOW;

#[derive(Clone, Copy)]
pub enum Tint {
    Red,
    Pink,
    White,
    Reset,
}

impl Tint {
    fn next(self) -> Self {
        match self {
            Tint::Red => Tint::Pink,
            Tint::Pink => Tint::White,
            Tint::White => Tint::Reset,
            Tint::Reset => Tint::Red,
        }
    }
}

pub struct TintAgent {
    config: config::Redshift,
    current_tint: Tint,
}

impl TintAgent {
    pub fn new(config: config::Redshift) -> Self {
        Self {
            config,
            current_tint: Tint::Reset,
        }
    }

    fn set_tint(&mut self, tint: Tint) {
        match Command::new("redshift")
            .args(self.config.args_for_tint(tint))
            .stdout(Stdio::piped())
            .stderr(Stdio::piped())
            .creation_flags(CREATE_NO_WINDOW)
            .output()
        {
            Err(err) => {
                error!(error = ?err, "could not run redshift");
            }
            Ok(output) if !output.status.success() => {
                error!(
                    stdout = &*String::from_utf8_lossy(&output.stdout),
                    stderr = &*String::from_utf8_lossy(&output.stderr),
                    "redshift error",
                )
            }
            Ok(_) => {}
        }
        self.current_tint = tint;
    }
}

impl Agent for TintAgent {
    fn handle(&mut self, signal: &Signal) {
        if let Signal::SetTint { tint } = *signal {
            self.set_tint(tint);
        } else if let Signal::CycleTint = *signal {
            let tint = self.current_tint.next();
            self.set_tint(tint);
        }
    }
}

impl config::Redshift {
    fn args_for_tint(&self, tint: Tint) -> Vec<&str> {
        let text = match tint {
            Tint::Red => &self.red,
            Tint::Pink => &self.pink,
            Tint::White => &self.white,
            Tint::Reset => &self.reset,
        };
        text.split_whitespace().collect()
    }
}
