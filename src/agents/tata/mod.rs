use crate::{
    agents::{
        tata::{
            fx::{FixedSizeBuffer, StereoToMono},
            real_fft::RealFFT,
        },
        Agent,
    },
    signal::Signal,
    windows::utils::increase_thread_priority,
};
use arrayvec::ArrayVec;
use cpal::{
    default_host,
    traits::{DeviceTrait, HostTrait, StreamTrait},
    SampleRate,
    Stream,
    StreamConfig,
};
use once_cell::unsync::OnceCell;
use ordered_float::NotNan;
use rustfft::algorithm::Radix4;
use std::error::Error;
use tracing::error;

mod fx;
mod real_fft;

pub struct TataAgent {
    // Keeping this alive keeps the audio stream from being closed.
    _stream: Option<Stream>,
}

impl TataAgent {
    pub fn new(sender: crossbeam_channel::Sender<Signal>) -> Self {
        let stream = start(
            move |edge, sound| {
                sender.send(Signal::TataSound(edge, sound)).unwrap();
            },
            None,
        )
        .map_err(|err| error!(error = ?err, "could not start listening"))
        .ok();
        Self { _stream: stream }
    }
}

impl Agent for TataAgent {}

const SAMPLE_RATE: u32 = 44100;
const FFT_LEN: usize = 4096;
const MAX_TRIGGERS: usize = 16;

fn start(
    mut f: impl FnMut(Edge, Sound) + Send + 'static,
    debug_info_sink: Option<DebugInfoSink<'static>>,
) -> Result<Stream, Box<dyn Error>> {
    let host = default_host();
    let device = host
        .default_input_device()
        .ok_or("no default input device")?;
    let mut to_mono = StereoToMono::new();
    let mut pipeline = Pipeline::new(SAMPLE_RATE);
    let increase_thread_priority_once = OnceCell::new();
    let stream = device.build_input_stream(
        &StreamConfig {
            channels: 2,
            sample_rate: SampleRate(SAMPLE_RATE),
        },
        move |data: &[f32]| {
            increase_thread_priority_once.get_or_init(increase_thread_priority);

            let data = to_mono.process(data);
            pipeline.process(data, &mut f, debug_info_sink);
        },
        |err| {
            error!(error = ?err, "stream error");
        },
    )?;
    stream.play()?;
    Ok(stream)
}

struct Pipeline {
    fixer: FixedSizeBuffer,
    recognizer: Recognizer,
    edge_detector: EdgeDetector,
}

impl Pipeline {
    pub fn new(sample_rate: u32) -> Self {
        Self {
            fixer: FixedSizeBuffer::new(FFT_LEN),
            recognizer: Recognizer::new(sample_rate),
            edge_detector: EdgeDetector::new(),
        }
    }

    pub fn process(
        &mut self,
        data: &[f32],
        f: &mut impl FnMut(Edge, Sound),
        debug_info_sink: Option<DebugInfoSink<'_>>,
    ) {
        let Self {
            fixer,
            recognizer,
            edge_detector,
        } = self;
        fixer.process(data, |data| {
            let sound = recognizer.process(data, debug_info_sink);
            for (edge, sound) in edge_detector.process(sound) {
                f(edge, sound);
            }
        })
    }
}

struct Recognizer {
    sample_rate: u32,
    fft: RealFFT<Radix4<f32>, f32>,
    fft_out: Vec<f32>,
}

impl Recognizer {
    fn new(sample_rate: u32) -> Self {
        Self {
            sample_rate,
            fft: RealFFT::new(Radix4::new(FFT_LEN, false)),
            fft_out: vec![0.0; FFT_LEN],
        }
    }

    fn process(
        &mut self,
        data: &[f32],
        debug_info_sink: Option<DebugInfoSink<'_>>,
    ) -> Option<Sound> {
        self.fft.process(data, &mut self.fft_out);
        let results: ArrayVec<[_; MAX_TRIGGERS]> = TRIGGERS
            .iter()
            .map(|t| (t.sound, t.evaluate(&self.fft_out, self.sample_rate)))
            .collect();
        let sound = results
            .iter()
            .filter(|&&(_g, s)| s >= 1.0)
            .max_by_key(|&&(_g, s)| NotNan::new(s).unwrap())
            .map(|&(g, _s)| g);

        if let Some(sink) = debug_info_sink {
            sink(DebugInfo {
                sound,
                results,
                fft: self.fft_out.clone(),
            });
        }

        sound
    }
}

struct EdgeDetector {
    last: Option<Sound>,
}

impl EdgeDetector {
    fn new() -> Self {
        Self { last: None }
    }

    fn process(&mut self, sound: Option<Sound>) -> ArrayVec<[(Edge, Sound); 2]> {
        if self.last == sound {
            return ArrayVec::new();
        }

        let result = match (self.last, sound) {
            (None, None) => arrayvec![],
            (None, Some(next)) => arrayvec![(Edge::Rise, next)],
            (Some(prev), None) => arrayvec![(Edge::Fall, prev)],
            (Some(prev), Some(next)) => arrayvec![(Edge::Fall, prev), (Edge::Rise, next)],
        };
        self.last = sound;
        result
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Edge {
    Rise,
    Fall,
}

#[allow(
    clippy::cast_possible_truncation,
    clippy::cast_precision_loss,
    clippy::cast_sign_loss
)]
fn fft_bin_index_for_frequency(sample_rate: u32, fft_size: usize, frequency: f32) -> usize {
    let bin_width = sample_rate as f32 / fft_size as f32;
    (frequency / bin_width).round() as usize
}

const TRIGGERS: &[Trigger<'_>] = &[
    Trigger::new(Sound::T, &[
        TriggerPart::new(1000.0, 1700.0, -1.0),
        TriggerPart::new(1700.0, 3000.0, 2.0),
    ]),
    Trigger::new(Sound::K, &[
        TriggerPart::new(1000.0, 1700.0, 1.0),
        TriggerPart::new(1700.0, 3000.0, -1.0),
    ]),
    Trigger::new(Sound::S, &[
        TriggerPart::new(1000.0, 3000.0, -1.0),
        TriggerPart::new(3000.0, 5000.0, 1.0),
    ]),
];

#[derive(Clone)]
struct Trigger<'a> {
    sound: Sound,
    parts: &'a [TriggerPart],
}

impl<'a> Trigger<'a> {
    pub const fn new(sound: Sound, parts: &'a [TriggerPart]) -> Self {
        Trigger { sound, parts }
    }

    fn evaluate(&self, fft: &[f32], sample_rate: u32) -> f32 {
        self.parts
            .iter()
            .map(|part| part.evaluate(fft, sample_rate))
            .sum()
    }
}

struct TriggerPart {
    low_freq: f32,
    high_freq: f32,
    strength: f32,
}

impl TriggerPart {
    pub const fn new(low_freq: f32, high_freq: f32, strength: f32) -> Self {
        TriggerPart {
            low_freq,
            high_freq,
            strength,
        }
    }

    fn evaluate(&self, fft: &[f32], sample_rate: u32) -> f32 {
        let low_index = fft_bin_index_for_frequency(sample_rate, fft.len(), self.low_freq);
        let high_index = fft_bin_index_for_frequency(sample_rate, fft.len(), self.high_freq);
        let bins = &fft[low_index..=high_index];
        let score = bins
            .iter()
            .map(|&x| NotNan::new(x).unwrap())
            .max()
            .unwrap()
            .into_inner();
        score * self.strength
    }
}

#[derive(Copy, Clone, Eq, PartialEq, Debug)]
pub enum Sound {
    T,
    K,
    S,
}

#[allow(dead_code)]
struct DebugInfo {
    sound: Option<Sound>,
    results: ArrayVec<[(Sound, f32); MAX_TRIGGERS]>,
    fft: Vec<f32>,
}

type DebugInfoSink<'a> = &'a (dyn Fn(DebugInfo) + Send + Sync);

#[cfg(test)]
mod tests {
    use super::*;
    use crate::agents::tata::fx::ConvertI16ToF32;
    use claxon::FlacReader;
    use parking_lot::Mutex;
    use std::{fmt, path::Path, sync::Arc};

    parameterized_test!(test, [
        t16("t16.flac", &[Sound::T; 16]),
        s16("s16.flac", &[Sound::S; 16]),
        #[ignore = "close enough"]
        k16("k16.flac", &[Sound::K; 16]),
    ]);

    fn test(filename: &str, expected: &[Sound]) {
        let here = Path::new(file!()).parent().unwrap();
        let path = here.join("fixtures").join(filename);
        let mut reader = FlacReader::open(path).unwrap();
        let sample_rate = reader.streaminfo().sample_rate;

        let mut convert_to_f32 = ConvertI16ToF32::new();
        let mut pipeline = Pipeline::new(sample_rate);

        let mut blocks = reader.blocks();
        let mut buffer = Vec::new();
        let mut samples = 0;

        let mut timestamps = Vec::new();
        let mut sounds = Vec::new();
        let mut debug_infos = Vec::new();

        while let Some(block) = blocks.read_next_or_eof(buffer).unwrap() {
            let data = block.channel(0);
            let data = convert_to_f32.process(data);
            let debug_info = Arc::new(Mutex::new(None));
            pipeline.process(
                data,
                &mut |edge, sound| {
                    if edge == Edge::Rise {
                        #[allow(clippy::cast_precision_loss)]
                        timestamps.push(samples as f32 / sample_rate as f32);
                        sounds.push(sound);
                        debug_infos.push(debug_info.lock().take().unwrap());
                    }
                },
                Some(&|di| {
                    *debug_info.lock() = Some(di);
                }),
            );

            samples += data.len();
            buffer = block.into_buffer();
        }
        assert_eq!(
            sounds,
            expected,
            "{}",
            FailMsg(&timestamps, &sounds, &debug_infos),
        );
    }

    struct FailMsg<'a>(&'a [f32], &'a [Sound], &'a [DebugInfo]);

    impl fmt::Display for FailMsg<'_> {
        fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
            let FailMsg(timestamps, sounds, debug_infos) = self;
            for ((timestamp, sound), debug_info) in
                timestamps.iter().zip(sounds.iter()).zip(debug_infos.iter())
            {
                write!(f, "{:.3} – {:?} (", timestamp, sound)?;

                let mut top = debug_info.results.clone();
                top.sort_unstable_by_key(|(_, x)| NotNan::new(-x).unwrap());
                for (i, (sound, score)) in top.iter().enumerate().take(3) {
                    if i > 0 {
                        write!(f, ", ")?;
                    }
                    write!(f, "{:.3} {:?}", score, sound)?;
                }
                writeln!(f, ")")?;
            }
            Ok(())
        }
    }
}

#[cfg(test)]
mod smoke_tests {
    use super::*;
    use glium::{backend::glutin::glutin::ContextBuilder, Display, Surface};
    use imgui::{im_str, Condition, Context, PlotHistogram, Ui, Window};
    use imgui_glium_renderer::Renderer;
    use imgui_winit_support::{HiDpiMode, WinitPlatform};
    use parking_lot::Mutex;
    use std::{
        fs,
        io,
        io::Write,
        sync::Arc,
        thread,
        time::{Duration, Instant},
    };
    use winit::{
        dpi::LogicalSize,
        event::{Event, WindowEvent},
        event_loop::{ControlFlow, EventLoop},
        platform::{desktop::EventLoopExtDesktop, windows::EventLoopExtWindows},
        window::WindowBuilder,
    };

    #[test]
    #[ignore = "smoke test"]
    fn smoke_test() {
        let cur_sound: Arc<Mutex<Option<Sound>>> = Arc::new(Mutex::new(None));
        let cur_debug_info: Arc<Mutex<Option<DebugInfo>>> = Arc::new(Mutex::new(None));
        thread::spawn({
            let cur_sound = cur_sound.clone();
            let cur_debug_info = cur_debug_info.clone();
            || {
                let _stream = start(
                    move |edge, sound| {
                        eprintln!("(edge, sound) = {:?}", (edge, sound));
                        let mut guard = cur_sound.lock();
                        match edge {
                            Edge::Rise => *guard = Some(sound),
                            Edge::Fall => *guard = None,
                        }
                    },
                    Some(Box::leak(Box::new(move |debug_info| {
                        *cur_debug_info.lock() = Some(debug_info);
                    }))),
                );
                thread::sleep(Duration::from_secs(u64::max_value()))
            }
        });

        run_imgui_window(|ui| {
            Window::new(im_str!("test"))
                .position([0.0, 0.0], Condition::Always)
                .size(ui.io().display_size, Condition::Always)
                .title_bar(false)
                .resizable(false)
                .build(ui, || {
                    let sound = *cur_sound.lock();

                    let guard = cur_debug_info.lock();
                    let debug_info = match &*guard {
                        Some(x) => x,
                        _ => return,
                    };

                    // Trim off the symmetric second half
                    let fft = &debug_info.fft[..debug_info.fft.len() / 2];
                    PlotHistogram::new(ui, <_>::default(), fft)
                        .graph_size([0.0, 96.0])
                        .scale_min(0.0)
                        .scale_max(1.0)
                        .build();

                    for (sound, score) in &debug_info.results {
                        ui.text(format!("{:?} – {:.2}", sound, score))
                    }

                    ui.text(format!("{:?}", sound));

                    if ui.button(im_str!("Dump"), <_>::default()) || sound.is_some() {
                        let mut file = fs::File::create(
                            dirs::desktop_dir().unwrap().join("helping-hands-debug.txt"),
                        )
                        .unwrap();
                        dump_debug_info(&mut file, &debug_info).unwrap();
                    }
                });
            ControlFlow::Wait
        });
    }

    fn dump_debug_info(w: &mut impl Write, debug_info: &DebugInfo) -> io::Result<()> {
        writeln!(w, "{:?}", debug_info.sound)?;
        for x in &debug_info.fft[..] {
            writeln!(w, "{}", x)?;
        }
        writeln!(w)?;
        for (sound, score) in &debug_info.results {
            writeln!(w, "{:?} – {}", sound, score)?;
        }
        Ok(())
    }

    fn run_imgui_window(mut draw: impl FnMut(&Ui<'_>) -> ControlFlow) {
        let builder = WindowBuilder::new()
            .with_title("test")
            .with_inner_size(LogicalSize::new(480.0, 360.0));
        let context = ContextBuilder::new();
        let mut event_loop: EventLoop<()> = EventLoop::new_any_thread();
        let display = Display::new(builder, context, &event_loop).unwrap();

        let mut imgui = Context::create();
        imgui.set_ini_filename(None);

        let style = imgui.style_mut();
        style.window_rounding = 0.0;
        style.window_border_size = 0.0;

        let mut platform = WinitPlatform::init(&mut imgui);
        platform.attach_window(
            imgui.io_mut(),
            display.gl_window().window(),
            HiDpiMode::Rounded,
        );

        let mut renderer = Renderer::init(&mut imgui, &display).unwrap();

        let mut last_frame = Instant::now();

        event_loop.run_return(move |event, _target, control_flow| {
            platform.handle_event(imgui.io_mut(), display.gl_window().window(), &event);

            match event {
                Event::NewEvents(_) => {
                    last_frame = imgui.io_mut().update_delta_time(last_frame);
                }
                Event::MainEventsCleared => {
                    platform
                        .prepare_frame(imgui.io_mut(), display.gl_window().window())
                        .unwrap();
                    display.gl_window().window().request_redraw();
                }
                Event::RedrawRequested(_) => {
                    let ui = imgui.frame();
                    if draw(&ui) == ControlFlow::Exit {
                        *control_flow = ControlFlow::Exit;
                        return;
                    }

                    platform.prepare_render(&ui, display.gl_window().window());
                    let draw_data = ui.render();

                    let mut target = display.draw();
                    target.clear_color(0.0, 0.0, 0.0, 0.0);
                    renderer.render(&mut target, draw_data).unwrap();
                    target.finish().unwrap();
                }
                Event::WindowEvent {
                    event: WindowEvent::CloseRequested,
                    ..
                } => {
                    *control_flow = ControlFlow::Exit;
                }
                _event => {}
            }
        });
    }
}
