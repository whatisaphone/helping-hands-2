/// Converts interleaved stereo samples to mono.
pub struct StereoToMono {
    buffer: Vec<f32>,
}

impl StereoToMono {
    pub fn new() -> Self {
        Self { buffer: Vec::new() }
    }

    pub fn process(&mut self, data: &[f32]) -> &[f32] {
        self.buffer.clear();
        self.buffer.reserve(data.len() / 2);

        let mut it = data.iter();
        // Iterate over adjacent interleaved samples.
        while let Some(x) = it.next() {
            let y = it.next().expect("the data length should be an even number");
            self.buffer.push((x + y) / 2.0);
        }
        &self.buffer[..]
    }
}
