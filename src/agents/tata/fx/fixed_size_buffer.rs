/// Groups samples into blocks of a fixed size.
pub struct FixedSizeBuffer {
    buffer: Vec<f32>,
    pos: usize,
}

impl FixedSizeBuffer {
    pub fn new(len: usize) -> Self {
        Self {
            buffer: vec![0.0; len],
            pos: 0,
        }
    }

    pub fn process(&mut self, mut buf: &[f32], mut f: impl FnMut(&[f32])) {
        while !buf.is_empty() {
            let chunk = usize::min(self.buffer.len() - self.pos, buf.len());
            self.buffer[self.pos..][..chunk].copy_from_slice(&buf[..chunk]);
            buf = &buf[chunk..];

            self.pos += chunk;
            if self.pos < self.buffer.len() {
                break;
            }

            f(&self.buffer);
            self.pos = 0;
        }
    }
}
