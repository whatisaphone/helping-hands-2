/// Converts `i16` samples, which are misleadingly stored in a `i32`, to `f32`
/// samples.
pub struct ConvertI16ToF32 {
    buffer: Vec<f32>,
}

impl ConvertI16ToF32 {
    pub fn new() -> Self {
        Self { buffer: Vec::new() }
    }

    #[allow(clippy::cast_precision_loss)]
    pub fn process(&mut self, data: &[i32]) -> &[f32] {
        self.buffer.clear();
        self.buffer
            .extend(data.iter().map(|&x| x as f32 / f32::from(i16::max_value())));
        &self.buffer
    }
}
