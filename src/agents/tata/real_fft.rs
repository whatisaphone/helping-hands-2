use rustfft::{num_complex::Complex, num_traits::Float, FFTnum, FFT};

pub struct RealFFT<F: FFT<T>, T: FFTnum> {
    fft: F,
    buf_in: Vec<Complex<T>>,
    buf_out: Vec<Complex<T>>,
}

impl<F: FFT<T>, T: FFTnum + Float> RealFFT<F, T> {
    pub fn new(fft: F) -> Self {
        let len = fft.len();
        Self {
            fft,
            buf_in: vec![Complex::new(T::zero(), T::zero()); len],
            buf_out: vec![Complex::new(T::zero(), T::zero()); len],
        }
    }

    #[allow(clippy::needless_range_loop)]
    pub fn process(&mut self, input: &[T], output: &mut [T]) {
        for i in 0..self.buf_in.len() {
            self.buf_in[i] = Complex::new(input[i], T::zero());
        }

        self.fft.process(&mut self.buf_in, &mut self.buf_out);

        for i in 0..output.len() {
            output[i] = self.buf_out[i].norm();
        }
    }
}
