use crate::{agents::Agent, broker::Broker, signal::Signal};
use std::thread;
use systray::Application;

pub struct SysTrayAgent;

impl SysTrayAgent {
    pub fn new(broker: Broker) -> Self {
        thread::spawn(|| make_systray(broker));
        Self
    }
}

impl Agent for SysTrayAgent {}

fn make_systray(broker: Broker) {
    let mut app = Application::new().unwrap();
    app.set_icon_from_resource(&"App".to_owned()).unwrap();
    app.set_tooltip(&"helping-hands".to_owned()).unwrap();
    app.add_menu_item(&"Quit".to_owned(), move |_| {
        broker.enqueue(Signal::Quit);
        Ok::<_, systray::Error>(())
    })
    .unwrap();
    app.wait_for_message().unwrap();
}
