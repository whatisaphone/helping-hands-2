use crate::{
    agents::Agent,
    broker::Broker,
    signal::Signal,
    windows::mouse_ll_hook::{Callback, Delta, MouseLLHook},
};
use winapi::um::winuser::WHEEL_DELTA;

pub struct MouseAgent;

impl MouseAgent {
    pub fn new(broker: Broker) -> Self {
        MouseLLHook::new(HookCallback::new(broker));
        Self
    }
}

impl Agent for MouseAgent {}

struct HookCallback {
    broker: Broker,
}

impl HookCallback {
    fn new(broker: Broker) -> Self {
        Self { broker }
    }
}

impl Callback for HookCallback {
    fn on_mouse_h_wheel(&mut self, delta: Delta) {
        if delta <= -WHEEL_DELTA {
            self.broker.enqueue(Signal::MouseWheelLeft);
        } else if delta >= WHEEL_DELTA {
            self.broker.enqueue(Signal::MouseWheelRight);
        }
    }
}
