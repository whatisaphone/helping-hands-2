use serde_derive::Deserialize;
use std::{fs, io::Read};
use toml;

#[derive(Default, Deserialize)]
pub struct Config {
    #[serde(default)]
    pub redshift: Redshift,
}

#[derive(Deserialize)]
pub struct Redshift {
    pub red: String,
    pub pink: String,
    pub white: String,
    #[serde(default = "Redshift::default_reset")]
    pub reset: String,
}

impl Default for Redshift {
    fn default() -> Self {
        Self {
            red: "-O 3500".to_owned(),
            pink: "-O 4500".to_owned(),
            white: "-O 5500".to_owned(),
            reset: Self::default_reset(),
        }
    }
}

impl Redshift {
    fn default_reset() -> String {
        "-x".to_owned()
    }
}

pub fn load() -> Result<Config, String> {
    let path = dirs::home_dir().unwrap().join(".helping-hands-2");
    let mut file = match fs::File::open(path) {
        Ok(file) => file,
        Err(_) => return Ok(Config::default()),
    };

    let mut contents = String::new();
    file.read_to_string(&mut contents)
        .map_err(|_| "Could not read config file")?;

    toml::from_str(&contents).map_err(|e| format!("Could not parse config: {}", e))
}
