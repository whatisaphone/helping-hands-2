use crate::{
    agents::{Edge, Sound, Tint},
    windows::inputsim::{MouseButton, MouseButtonDirection, VirtualKey},
};

pub enum Signal {
    CycleTint,
    ForegroundWindowChanged { exe: String },
    KeyboardChord { vks: &'static [VirtualKey] },
    MouseWheelLeft,
    MouseWheelRight,
    JoyButtonDown { button: u8 },
    Quit,
    SetTint { tint: Tint },
    SimulateMouseButton(MouseButton, MouseButtonDirection),
    TataSound(Edge, Sound),
}
