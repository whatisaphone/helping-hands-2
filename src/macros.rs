macro_rules! arrayvec {
    ($($x:expr),* $(,)?) => {{
        #[allow(unused_mut)]
        let mut result = ::arrayvec::ArrayVec::new();
        $(result.push($x);)*
        result
    }};
}

macro_rules! regex {
    ($regex:literal) => {{
        use once_cell::sync::Lazy;
        use regex::Regex;

        static REGEX: Lazy<Regex> = Lazy::new(|| Regex::new($regex).unwrap());
        &*REGEX
    }};
}

#[cfg(test)]
macro_rules! parameterized_test {
    ($fn:ident, [
        $(
            $(#[$attr:meta])* $name:ident($($arg:expr),* $(,)?)
        ),*
        $(,)?
    ]) => {
        mod $fn {
            use super::*;

            $(
                #[test]
                $(#[$attr])*
                fn $name() {
                    $fn($($arg),*)
                }
            )*
        }
    };
}
