#![windows_subsystem = "windows"]
#![warn(future_incompatible, rust_2018_compatibility, rust_2018_idioms, unused)]
#![warn(clippy::pedantic)]
#![allow(
    clippy::module_name_repetitions,
    clippy::non_ascii_literal,
    clippy::single_match,
    clippy::single_match_else
)]
#![cfg_attr(feature = "strict", deny(warnings))]

use crate::{
    agents::{
        Agent,
        ContextAgent,
        ForegroundWindowAgent,
        GamepadAgent,
        InputSimulateAgent,
        MouseAgent,
        SysTrayAgent,
        TataAgent,
        TintAgent,
    },
    broker::Broker,
    signal::Signal,
    windows::utils::increase_thread_priority,
};
use vec_box::vec_box;

#[macro_use]
mod macros;

mod agents;
mod broker;
mod config;
mod signal;
mod windows;

fn main() {
    tracing_subscriber::fmt::fmt()
        .with_env_filter(tracing_subscriber::EnvFilter::from_default_env())
        .init();

    let config = match config::load() {
        Ok(config) => config,
        Err(msg) => {
            println!("{}", msg);
            return;
        }
    };

    // The message bus simulates keyboard/mouse events, and that should stay
    // responsive even when the CPU is at 100%.
    increase_thread_priority();

    let (tx, rx) = crossbeam_channel::unbounded();
    let mut agents: Vec<Box<dyn Agent>> = vec_box![
        ContextAgent::new(Broker::new(tx.clone())),
        ForegroundWindowAgent::new(Broker::new(tx.clone())),
        GamepadAgent::new(Broker::new(tx.clone())),
        InputSimulateAgent::new(),
        MouseAgent::new(Broker::new(tx.clone())),
        SysTrayAgent::new(Broker::new(tx.clone())),
        TataAgent::new(tx),
        TintAgent::new(config.redshift),
    ];
    pump(&mut agents, rx);
}

fn pump(agents: &mut [Box<dyn Agent>], rx: crossbeam_channel::Receiver<Signal>) {
    for signal in rx {
        for agent in agents.iter_mut() {
            agent.handle(&signal);
        }
        if let Signal::Quit = signal {
            break;
        }
    }
}
