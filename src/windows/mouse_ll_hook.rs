use crate::windows::utils::run_message_loop;
use scopeguard::defer;
use std::{convert::TryFrom, io, mem, ptr, thread};
use tracing::error;
use winapi::{
    ctypes::c_int,
    shared::minwindef::{LPARAM, LRESULT, UINT, WPARAM},
    um::winuser::{
        CallNextHookEx,
        SetWindowsHookExW,
        UnhookWindowsHookEx,
        HC_ACTION,
        MSLLHOOKSTRUCT,
        WH_MOUSE_LL,
        WM_MOUSEHWHEEL,
    },
};

pub type Delta = i16;

pub struct MouseLLHook;

impl MouseLLHook {
    pub fn new(callback: impl Callback + 'static) -> Self {
        thread::spawn(|| Self::hook_thread(callback));
        Self
    }

    fn hook_thread(mut callback: impl Callback + 'static) {
        unsafe {
            assert!(THERE_CAN_ONLY_BE_ONE.is_none());
            THERE_CAN_ONLY_BE_ONE = Some(&mut callback);
            defer! {
                THERE_CAN_ONLY_BE_ONE = None;
            }

            let hook =
                SetWindowsHookExW(WH_MOUSE_LL, Some(global_hook_callback), ptr::null_mut(), 0);
            if hook.is_null() {
                error!(
                    error = ?io::Error::last_os_error(),
                    "could not create WH_MOUSE_LL hook",
                );
                return;
            }
            defer! {
                if UnhookWindowsHookEx(hook) == 0 {
                    error!(
                        error = ?io::Error::last_os_error(),
                        "could not unhook WH_MOUSE_LL hook",
                    );
                }
            }

            run_message_loop();
        }
    }
}

pub trait Callback: Send {
    fn on_mouse_h_wheel(&mut self, delta: Delta);
}

static mut THERE_CAN_ONLY_BE_ONE: Option<*mut dyn Callback> = None;

#[allow(non_snake_case)]
unsafe extern "system" fn global_hook_callback(
    nCode: c_int,
    wParam: WPARAM,
    lParam: LPARAM,
) -> LRESULT {
    if nCode < 0 {
        return CallNextHookEx(ptr::null_mut(), nCode, wParam, lParam);
    }

    if nCode == HC_ACTION {
        let message = UINT::try_from(wParam).unwrap();
        let data = mem::transmute::<LPARAM, &MSLLHOOKSTRUCT>(lParam);

        match message {
            WM_MOUSEHWHEEL => {
                #[allow(clippy::cast_possible_wrap)]
                let delta = Delta::try_from((data.mouseData as i32) >> 16).unwrap();
                (*THERE_CAN_ONLY_BE_ONE.unwrap()).on_mouse_h_wheel(delta);
                return 1; // suppress event
            }
            _ => {}
        }
    }

    0 // allow event through
}
