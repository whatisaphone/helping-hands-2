use crate::windows::utils::run_message_loop;
use scopeguard::defer;
use std::{io, ptr, thread};
use tracing::error;
use winapi::{
    shared::{
        minwindef::DWORD,
        ntdef::LONG,
        windef::{HWINEVENTHOOK, HWND},
    },
    um::winuser::{
        SetWinEventHook,
        UnhookWinEvent,
        EVENT_SYSTEM_FOREGROUND,
        WINEVENT_OUTOFCONTEXT,
    },
};

pub struct ForegroundWindowHook;

impl ForegroundWindowHook {
    pub fn new(callback: impl Callback + 'static) -> Self {
        thread::spawn(|| Self::hook_thread(callback));
        Self
    }

    fn hook_thread(mut callback: impl Callback + 'static) {
        unsafe {
            assert!(THERE_CAN_ONLY_BE_ONE.is_none());
            THERE_CAN_ONLY_BE_ONE = Some(&mut callback);
            defer! {
                THERE_CAN_ONLY_BE_ONE = None;
            }

            let hook = SetWinEventHook(
                EVENT_SYSTEM_FOREGROUND,
                EVENT_SYSTEM_FOREGROUND,
                ptr::null_mut(),
                Some(global_hook_callback),
                0,
                0,
                WINEVENT_OUTOFCONTEXT,
            );
            if hook.is_null() {
                error!(
                    error = ?io::Error::last_os_error(),
                    "could not create EVENT_SYSTEM_FOREGROUND hook",
                );
                return;
            }
            defer! {
                if UnhookWinEvent(hook) == 0 {
                    error!(
                        error = ?io::Error::last_os_error(),
                        "could not unhook EVENT_SYSTEM_FOREGROUND hook",
                    );
                }
            }

            run_message_loop();
        }
    }
}

pub trait Callback: Send {
    fn foreground_window_changed(&mut self, hwnd: HWND);
}

static mut THERE_CAN_ONLY_BE_ONE: Option<*mut dyn Callback> = None;

#[allow(non_snake_case)]
unsafe extern "system" fn global_hook_callback(
    _hWinEventHook: HWINEVENTHOOK,
    _event: DWORD,
    hwnd: HWND,
    _idObject: LONG,
    _idChild: LONG,
    _dwEventThread: DWORD,
    _dwmsEventTime: DWORD,
) {
    (*THERE_CAN_ONLY_BE_ONE.unwrap()).foreground_window_changed(hwnd);
}
