use crate::agents::Edge;
use std::{convert::TryInto, io, mem, thread::sleep, time::Duration};
use tracing::error;
use winapi::{
    ctypes::c_int,
    shared::minwindef::DWORD,
    um::winuser::{
        SendInput,
        INPUT,
        INPUT_KEYBOARD,
        INPUT_MOUSE,
        KEYEVENTF_KEYUP,
        MOUSEEVENTF_LEFTDOWN,
        MOUSEEVENTF_LEFTUP,
        MOUSEEVENTF_MIDDLEDOWN,
        MOUSEEVENTF_MIDDLEUP,
        MOUSEEVENTF_RIGHTDOWN,
        MOUSEEVENTF_RIGHTUP,
    },
};

pub type VirtualKey = c_int;

pub fn send_chord(vks: &[VirtualKey]) {
    // the delays prevent weird behavior

    for &vk in vks {
        key_down(vk);
        sleep(Duration::from_millis(10));
    }

    sleep(Duration::from_millis(10));

    for &vk in vks.iter().rev() {
        key_up(vk);
        sleep(Duration::from_millis(10));
    }
}

pub fn send_mouse(button: MouseButton, direction: MouseButtonDirection) {
    send_input(mouse_input(button, direction));
}

fn key_down(vk: VirtualKey) {
    send_input(key_input(vk, 0));
}

fn key_up(vk: VirtualKey) {
    send_input(key_input(vk, KEYEVENTF_KEYUP));
}

fn key_input(vk: VirtualKey, flags: DWORD) -> INPUT {
    unsafe {
        let mut input = INPUT {
            type_: INPUT_KEYBOARD,
            u: mem::zeroed(),
        };
        input.u.ki_mut().wVk = vk.try_into().unwrap();
        input.u.ki_mut().dwFlags = flags;
        input
    }
}

fn mouse_input(button: MouseButton, direction: MouseButtonDirection) -> INPUT {
    unsafe {
        let mut input = INPUT {
            type_: INPUT_MOUSE,
            u: mem::zeroed(),
        };
        input.u.mi_mut().dwFlags = match (button, direction) {
            (MouseButton::Left, MouseButtonDirection::Down) => MOUSEEVENTF_LEFTDOWN,
            (MouseButton::Left, MouseButtonDirection::Up) => MOUSEEVENTF_LEFTUP,
            (MouseButton::Right, MouseButtonDirection::Down) => MOUSEEVENTF_RIGHTDOWN,
            (MouseButton::Right, MouseButtonDirection::Up) => MOUSEEVENTF_RIGHTUP,
            (MouseButton::Middle, MouseButtonDirection::Down) => MOUSEEVENTF_MIDDLEDOWN,
            (MouseButton::Middle, MouseButtonDirection::Up) => MOUSEEVENTF_MIDDLEUP,
        };
        input
    }
}

fn send_input(mut input: INPUT) {
    let result = unsafe { SendInput(1, &mut input, mem::size_of::<INPUT>().try_into().unwrap()) };
    if result != 1 {
        error!(error = ?io::Error::last_os_error(), "could not send input");
    }
}

#[derive(Copy, Clone)]
pub enum MouseButton {
    Left,
    Right,
    Middle,
}

#[derive(Copy, Clone)]
pub enum MouseButtonDirection {
    Down,
    Up,
}

impl From<Edge> for MouseButtonDirection {
    fn from(edge: Edge) -> Self {
        match edge {
            Edge::Rise => Self::Down,
            Edge::Fall => Self::Up,
        }
    }
}
