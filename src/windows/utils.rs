use scopeguard::defer;
use std::{convert::TryInto, error::Error, io, mem, ptr, slice};
use tracing::error;
use winapi::{
    shared::{minwindef::MAX_PATH, ntdef::WCHAR, windef::HWND},
    um::{
        handleapi::CloseHandle,
        processthreadsapi::{GetCurrentThread, OpenProcess, SetThreadPriority},
        psapi::GetModuleFileNameExW,
        winbase::THREAD_PRIORITY_ABOVE_NORMAL,
        winnt::PROCESS_QUERY_INFORMATION,
        winuser::{DispatchMessageW, GetMessageW, GetWindowThreadProcessId, TranslateMessage},
    },
};

pub fn run_message_loop() {
    unsafe {
        let mut msg = mem::MaybeUninit::uninit();
        loop {
            let ret = GetMessageW(msg.as_mut_ptr(), ptr::null_mut(), 0, 0);
            if ret == -1 {
                error!(error = ?io::Error::last_os_error(), "message loop error");
            }
            if ret == 0 {
                break;
            }
            TranslateMessage(msg.as_ptr());
            DispatchMessageW(msg.as_ptr());
        }
    }
}

pub fn increase_thread_priority() {
    unsafe {
        let thread = GetCurrentThread();
        if SetThreadPriority(thread, THREAD_PRIORITY_ABOVE_NORMAL.try_into().unwrap()) == 0 {
            error!(error = ?io::Error::last_os_error(), "error setting thread priority");
        }
    }
}

#[allow(non_snake_case)]
pub fn get_exe_for_hwnd(hWnd: HWND) -> Result<String, Box<dyn Error>> {
    unsafe {
        let mut process_id = 0;
        let thread_id = GetWindowThreadProcessId(hWnd, &mut process_id);
        if thread_id == 0 {
            return Err(Box::new(io::Error::last_os_error()));
        }

        let hProcess = OpenProcess(PROCESS_QUERY_INFORMATION, 0, process_id);
        if hProcess.is_null() {
            return Err(Box::new(io::Error::last_os_error()));
        }
        defer! {
            if CloseHandle(hProcess) == 0 {
                error!(
                    error = ?io::Error::last_os_error(),
                    "could not close process handle",
                );
            }
        }

        let mut exe: mem::MaybeUninit<[WCHAR; MAX_PATH]> = mem::MaybeUninit::uninit();
        let exe_len = GetModuleFileNameExW(
            hProcess,
            ptr::null_mut(),
            exe.as_mut_ptr() as *mut WCHAR,
            MAX_PATH.try_into().unwrap(),
        );
        if exe_len == 0 {
            return Err(Box::new(io::Error::last_os_error()));
        }
        let exe = slice::from_raw_parts(exe.as_mut_ptr() as *mut WCHAR, exe_len as usize);
        Ok(String::from_utf16(exe).map_err(|_| "filename is not valid UTF-16")?)
    }
}
