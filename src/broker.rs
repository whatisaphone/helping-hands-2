use crate::signal::Signal;

pub struct Broker {
    sender: crossbeam_channel::Sender<Signal>,
}

impl Broker {
    pub fn new(sender: crossbeam_channel::Sender<Signal>) -> Self {
        Self { sender }
    }

    pub fn enqueue(&self, signal: Signal) {
        self.sender.send(signal).unwrap();
    }
}
